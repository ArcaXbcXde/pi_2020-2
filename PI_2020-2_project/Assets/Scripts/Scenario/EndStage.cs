﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndStage : MonoBehaviour {
	
	private GameManager gameManager;

	public GameObject endScreen;

	public Text textTitle;

	public Text textTime;
	
	public Text textBestTime;

	public Text textParTime;

	public Text textPoints;

	public Text textBestPoints;

	public Text textParPoints;
	
	//public PlayerCarControl playerControl;
	public PlayerBallControl playerControl;

	private void Start () {

		gameManager = GameManager.Instance;
	}

	private void OnTriggerEnter (Collider col) {

		if (col.tag == "Player") {

			// Stops all actions from the player in-game
			playerControl.enabled = false;
			
			col.GetComponent<Rigidbody>().velocity = Vector3.zero;
			col.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;


			// Saves if this stage is the last unlocked stage
			if (gameManager.currentLevel == gameManager.currentLevelProgress) {

				gameManager.currentLevelProgress++;
				PlayerPrefs.SetInt("Level", gameManager.currentLevelProgress);
			}

			// Saves this level time if its lower than the last best
			if (gameManager.currentLevelTime < gameManager.currentLevelBestTime) {

				PlayerPrefs.SetFloat(gameManager.currentLevel + "Time", gameManager.currentLevelTime);
				gameManager.currentLevelBestTime = gameManager.currentLevelTime;
			}
			
			// Saves this level points if its bigger than the last best
			if (gameManager.currentLevelPoints > gameManager.currentLevelBestPoints) {

				PlayerPrefs.SetInt(gameManager.currentLevel + "Points", gameManager.currentLevelPoints);
				gameManager.currentLevelBestPoints = gameManager.currentLevelPoints;
			}

			// Changes text while not showing
			WriteEndStatistics();

			gameManager.currentLevel++;

			// After saving level info and progress and changing text, zeroes points and time for next stage
			gameManager.ZeroPoints();

			// Shows cursor
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = true;

			// Isn't playing on a stage anymore
			gameManager.onStage = false;

			// Show end screen
			endScreen.SetActive(true);

		}
	}

	private void WriteEndStatistics () {

		textTitle.text = ("Stage " + gameManager.currentLevel + " Completed!");

		textTime.text = ("Time: " + gameManager.TimeFormat(gameManager.currentLevelHours, gameManager.currentLevelMinutes, gameManager.currentLevelSeconds));
		
		textBestTime.text = ("Best: " + gameManager.ReturnTimeFormatted(gameManager.currentLevelBestTime));

		textParTime.text = ("Par: " + gameManager.ReturnTimeFormatted(gameManager.currentLevelParTime));

		textPoints.text = ("Points: " + gameManager.currentLevelPoints.ToString());

		textBestPoints.text = ("Best: " + gameManager.currentLevelBestPoints.ToString());

		textParPoints.text = ("Par: " + gameManager.currentLevelParPoints.ToString());
	}
}