﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KillY : MonoBehaviour {

	private const float OFFSET = 1.0f;

	private void OnTriggerEnter (Collider col) {

		if (col.tag == "Player") {
			
			col.GetComponent<Transform>().SetPositionAndRotation(GameManager.Instance.actualCheckpoint.transform.position + (Vector3.up * OFFSET), new Quaternion (0.0f, 0.0f, 0.0f, 0.0f));

			col.GetComponent<Rigidbody>().angularVelocity = new Vector3 (0.0f, 0.0f, 0.0f);
			col.GetComponent<Rigidbody>().velocity = new Vector3 (0.0f, 0.0f, 0.0f);
		}

		if (col.tag == "Prop") {

			if (col.GetComponent<ResetPosition>() == null) {

				Destroy (col.gameObject);
			}
		}
	}
}