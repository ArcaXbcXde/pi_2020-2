﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckGround : MonoBehaviour {
	
	private int groundAmount = 0;

	private void OnTriggerEnter (Collider col) {
		if (col.tag == "Ground" || col.tag == "Prop") {

			groundAmount ++;
		}
	}

	private void OnTriggerExit (Collider col) {
		if (col.tag == "Ground" || col.tag == "Prop") {

			groundAmount --;
		}
	}

	public bool IsOnGround () {
		if (groundAmount > 0) {

			return true;
		} else {

			return false;
		}
	}
}