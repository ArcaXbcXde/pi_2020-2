﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : Entities {
	
	public Text timeText;

	public Text pointsText;

	[HideInInspector]
	public Transform myHolder;

	private GameManager gameManager;

	private void Start () {

		gameManager = GameManager.Instance;

		pointsText.text = "Points: " + 0;
	}
	
	public void AddPoint () {

		gameManager.currentLevelPoints += 1;

		pointsText.text = "Points: " + gameManager.currentLevelPoints;
	}

	private void OnEnable () {

		timeText.text = "Time: 00:00";
		pointsText.text = "Points: 0";
	}

	private void Update () {
		
		timeText.text = gameManager.TimeFormat(gameManager.currentLevelHours, gameManager.currentLevelMinutes, gameManager.currentLevelSeconds);
	}
}