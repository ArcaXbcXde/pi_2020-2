﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetAxisY : MonoBehaviour {

	private Toggle thisToggle;

	private void Awake () {

		thisToggle = GetComponent<Toggle>();

		thisToggle.isOn = GameManager.Instance.invertYAxis;
		thisToggle.onValueChanged.AddListener(delegate { ValueChangeCheck(); });
	}
	
    private void ValueChangeCheck () {

		GameManager.Instance.invertYAxis = thisToggle.isOn;
	}
}