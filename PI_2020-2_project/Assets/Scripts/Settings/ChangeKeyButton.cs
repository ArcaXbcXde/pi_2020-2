﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Keys for every non-axis related action of the player
/// </summary>
public enum PlayerAction {
	Jump,
	Dash,
	Drift
}

public class ChangeKeyButton : MonoBehaviour {

	[Tooltip ("What action this key changes?")]
	public PlayerAction thisButtonKey;

	[Tooltip ("To change player's keys")]
	public PlayerBallControl playerKeys;

	// If the button is waiting any keypress
	private bool waitingKey = false;

	// The child text of the button
	private Text buttonText;

	private GameManager manager;

	// The key to substitute the old key
	private KeyCode keyPressed;

	private void Start () {
		
		manager = GameManager.Instance;

		buttonText = transform.GetChild(0).GetComponent<Text>();

		if (thisButtonKey == PlayerAction.Jump) {
			
			buttonText.text = playerKeys.jumpKey.ToString();
			keyPressed = manager.jumpKey;
		} else if (thisButtonKey == PlayerAction.Dash) {

			buttonText.text = playerKeys.dashKey.ToString();
			keyPressed = manager.dashKey;
		}
	}

	private void Update () {
		
		if ((waitingKey == true) && Input.anyKeyDown) {

			keyPressed = WhatButtonWasPressed();

			if (thisButtonKey == PlayerAction.Jump) {

				manager.ChangeJumpKey (keyPressed);
			} else if(thisButtonKey == PlayerAction.Dash) {

				manager.ChangeDashKey (keyPressed);
			} else if (thisButtonKey == PlayerAction.Drift) {

				manager.ChangeDriftKey (keyPressed);
			}
			
			playerKeys.UpdateKeys();

			buttonText.text = keyPressed.ToString();

			waitingKey = false;
		}
	}

	// Goes through every possible key in the KeyCode enum to identify what was pressed
	private KeyCode WhatButtonWasPressed () {
		
		foreach (KeyCode key in System.Enum.GetValues(typeof(KeyCode))) {
			if (Input.GetKeyDown(key)) {
				
				return key;
			}
		}
		return keyPressed;
	}

	// Button call this method when clicked
	public void Clicked () {

		buttonText.text = "Press key";
		waitingKey = true;
	}

	public void BackToDefault () {

		if (thisButtonKey == PlayerAction.Jump) {

			manager.ChangeJumpKey(playerKeys.defaultJumpKey);
			buttonText.text = playerKeys.defaultJumpKey.ToString();

		} else if (thisButtonKey == PlayerAction.Dash) {

			manager.ChangeDashKey(playerKeys.defaultDashKey);
			buttonText.text = playerKeys.defaultDashKey.ToString();
		}

		playerKeys.UpdateKeys();
	}
}