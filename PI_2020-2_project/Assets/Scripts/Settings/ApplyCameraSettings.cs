﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using System;

public class ApplyCameraSettings : MonoBehaviour {

	private float initialSpeedX;
	private float initialSpeedY;

	private CinemachineFreeLook thisCinemachine;

	private void Start () {

		thisCinemachine = GetComponent<CinemachineFreeLook>();

		initialSpeedX = thisCinemachine.m_XAxis.m_MaxSpeed;
		initialSpeedY = thisCinemachine.m_YAxis.m_MaxSpeed;

		ApplyNewSettings();

	}

	public void ApplyNewSettings () {

		thisCinemachine.m_XAxis.m_MaxSpeed = initialSpeedX * GameManager.Instance.cameraSensitivity;
		thisCinemachine.m_YAxis.m_MaxSpeed = initialSpeedY * GameManager.Instance.cameraSensitivity;

		thisCinemachine.m_XAxis.m_InvertInput = GameManager.Instance.invertXAxis;
		thisCinemachine.m_YAxis.m_InvertInput = GameManager.Instance.invertYAxis;
	}
}