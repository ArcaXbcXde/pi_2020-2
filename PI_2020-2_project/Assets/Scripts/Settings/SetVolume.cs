﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetVolume : MonoBehaviour {

	public Text textToSet;

	private Slider thisSlider;

	private void Awake () {

		thisSlider = GetComponent<Slider>();

		thisSlider.value = GameManager.Instance.soundVolume;
		thisSlider.onValueChanged.AddListener(delegate { ValueChangeCheck(); });
	}

	public void ValueChangeCheck () {

		GameManager.Instance.soundVolume = thisSlider.value;
		textToSet.text = (thisSlider.value * 100).ToString("f0") + "%";
	}
}
