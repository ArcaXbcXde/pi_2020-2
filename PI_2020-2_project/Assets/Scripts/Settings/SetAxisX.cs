﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetAxisX : MonoBehaviour {

	private Toggle thisToggle;

	private void Awake () {

		thisToggle = GetComponent<Toggle>();

		thisToggle.isOn = GameManager.Instance.invertXAxis;
		thisToggle.onValueChanged.AddListener(delegate { ValueChangeCheck(); });
	}

	private void ValueChangeCheck () {

		GameManager.Instance.invertXAxis = thisToggle.isOn;
	}
}