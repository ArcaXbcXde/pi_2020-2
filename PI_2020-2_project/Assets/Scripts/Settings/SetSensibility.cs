﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetSensibility : MonoBehaviour {

	public Text textToSet;

	private Slider thisSlider;
	
	private void Awake () {
		
		thisSlider = GetComponent<Slider>();

		thisSlider.value = GameManager.Instance.cameraSensitivity;
		thisSlider.onValueChanged.AddListener(delegate { ValueChangeCheck(); });
	}

	public void ValueChangeCheck () {

		GameManager.Instance.cameraSensitivity = thisSlider.value;
		textToSet.text = "x" + thisSlider.value.ToString("f2");
	}
}