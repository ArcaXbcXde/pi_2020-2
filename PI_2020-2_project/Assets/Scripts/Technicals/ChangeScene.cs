﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour {

	private void Start () {
		
		if (SceneManager.GetActiveScene().name == "StartMenu") {

			GameManager.Instance.LoadOptions();
		}
	}

	public void ChangeTo (string sceneName) {

		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
		SceneManager.LoadScene(sceneName);
	}

	public void RestartScene () {

		GameManager.Instance.EnterStage();
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	public void QuitGame () {

		Application.Quit();
	}
}
