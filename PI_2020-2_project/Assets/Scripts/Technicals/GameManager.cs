﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	#region Options

	public float soundVolume = 1.0f;

	public float cameraSensitivity = 1.0f;

	public bool invertXAxis = false;

	public bool invertYAxis = true;

	#endregion

	#region Stage variables

	[HideInInspector]
	public bool onStage = true;

	//[HideInInspector]
	public int currentLevel;

	//[HideInInspector]
	public int currentLevelProgress = 1;

	//[HideInInspector]
	public int currentLevelParPoints = 0;

	//[HideInInspector]
	public int currentLevelBestPoints = 0;

	//[HideInInspector]
	public int currentLevelPoints = 0;
	
	//[HideInInspector]
	public int currentLevelHours = 0;

	//[HideInInspector]
	public int currentLevelMinutes = 0;

	//[HideInInspector]
	public float currentLevelSeconds = 0.0f;
	
	//[HideInInspector]
	public float currentLevelParTime = 0.0f;

	//[HideInInspector]
	public float currentLevelTime = 999.9f;

	//[HideInInspector]
	public float currentLevelBestTime = 999.9f;

	//[HideInInspector]
	public CheckpointControl actualCheckpoint;

	#endregion

	#region Key variables

	[HideInInspector]
	public KeyCode jumpKey;

	[HideInInspector]
	public KeyCode dashKey;

	[HideInInspector]
	public KeyCode driftKey;

	#endregion

	public static GameManager Instance {

		get;
		private set;
	}

	#region Basic methods

	private void Awake () {

		InitialSet();
		currentLevelProgress = PlayerPrefs.GetInt("Level", 1);

		ZeroPoints();

		LoadOptions();
	}

	private void Update () {
		
		if (onStage == true) {

			currentLevelTime += Time.deltaTime;
			currentLevelSeconds += Time.deltaTime;
			if (currentLevelSeconds >= 60.0f) {
				currentLevelSeconds -= 60.0f;
				currentLevelMinutes += 1;

				if (currentLevelMinutes >= 60) {

					currentLevelMinutes -= 60;
					currentLevelHours += 1;
				}
			}
		}
	}

	#endregion

	// Guarantee that there is one and only one instance of this, and then sets this as its own singleton instance
	private void InitialSet () {

		if (Instance == null) {

			Instance = this;
			DontDestroyOnLoad(gameObject);
		} else {

			Destroy(gameObject);
		}
	}

	#region Key methods

	public void ChangeJumpKey (KeyCode newKey) {

		jumpKey = newKey;
	}

	public void ChangeDashKey (KeyCode newKey) {

		dashKey = newKey;
	}

	public void ChangeDriftKey (KeyCode newKey) {

		driftKey = newKey;
	}

	#endregion

	#region Utility methods

	public void EnterStage () {

		onStage = true;
	}

	public void NextStage () {

		onStage = true;
	}

	public void ZeroPoints () {

		currentLevelSeconds = 0.0f;
		currentLevelMinutes = 0;
		currentLevelHours = 0;
		currentLevelTime = 0.0f;
		currentLevelPoints = 0;
	}

	public string TimeFormat (int hours, int minutes, float seconds) {

		if (hours == 0) {
			if (seconds > 10) {

				return (minutes + ":" + seconds.ToString("f1"));
			} else {

				return (minutes + ":0" + seconds.ToString("f1"));
			}
		} else {
			if (minutes > 10) {

				if (seconds > 10) {

					return (hours + ":" + minutes + ":" + seconds.ToString("f1"));
				} else {

					return (hours + ":" + minutes + ":0" + seconds.ToString("f1"));
				}
			} else {

				if (seconds > 10) {

					return (hours + ":0" + minutes + ":" + seconds.ToString("f1"));
				} else {

					return (hours + ":0" + minutes + ":0" + seconds.ToString("f1"));
				}
			}
		}
	}

	public string ReturnTimeFormatted (float seconds) {
		
		int minutes = 0;
		int hours = 0;

		while (currentLevelParTime > 60) {

			currentLevelParTime -= 60;
			minutes++;
			if (minutes >= 60) {

				minutes -= 60;
				hours++;
			}
		}

		return TimeFormat(hours, minutes, seconds);
	}

	public void SaveOptions () {

		PlayerPrefs.SetFloat ("SoundVolume", soundVolume);
		PlayerPrefs.SetFloat ("CameraSensitivity", cameraSensitivity);

		if (invertXAxis == true) {

			PlayerPrefs.SetInt ("InvertXAxis", 1);
		}else {

			PlayerPrefs.SetInt("InvertXAxis", 0);
		}

		if (invertYAxis == true) {

			PlayerPrefs.SetInt("InvertYAxis", 1);
		} else {

			PlayerPrefs.SetInt("InvertYAxis", 0);
		}
	}

	public void LoadOptions () {
		soundVolume = PlayerPrefs.GetFloat("SoundVolume", 1);
		soundVolume = PlayerPrefs.GetFloat("CameraSensitivity", 1);

		int InvertAxisInt = PlayerPrefs.GetInt("InvertXAxis", 0);
		if (InvertAxisInt == 1) {

			invertXAxis = true;
		} else {

			invertXAxis = false;
		}

		InvertAxisInt = PlayerPrefs.GetInt("InvertYAxis", 1);
		if (InvertAxisInt == 1) {

			invertYAxis = true;
		} else {

			invertYAxis = false;
		}
	}

	#endregion

	// Add a possibility to delete all saved information from the game, and also an option to the inspector to do it (last option of the 3 dots menu) 
	[ContextMenu ("Reset save file")]
	public void ResetSaveFile () {

		PlayerPrefs.DeleteAll();
	}
}