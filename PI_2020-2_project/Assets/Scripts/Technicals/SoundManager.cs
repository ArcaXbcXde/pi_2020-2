﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

	public Transform mainCamera;

	public AudioSource bgSound;

	public AudioSource soundSeed;
	
	public AudioSource soundMenuAccept;
	
	public AudioSource soundMenuDecline;
		
	public AudioSource soundOutOfTime;

	public AudioSource soundSliderTick;

	public AudioSource soundButtonTick;

	public static SoundManager Instance {

		get;
		private set;
	}

	private GameManager manager;

	private void Awake () {

		InitialSet();
	}

	private void Start () {
		
		manager = GameManager.Instance;
	}

	private void LateUpdate () {
		
		transform.position = mainCamera.position;

		// Urgently optimize
		bgSound.volume = GameManager.Instance.soundVolume;
	}

	private void InitialSet () {

		if (Instance == null) {

			Instance = this;
		}
	}

	public void PlayMenuAccept () {
		
		soundMenuAccept.volume = manager.soundVolume;
		soundMenuAccept.Play();
	}

	public void PlayMenuDecline () {

		soundMenuDecline.volume = manager.soundVolume;
		soundMenuDecline.Play();
	}

	public void PlaySliderTick () {

		soundSliderTick.volume = manager.soundVolume;
		soundSliderTick.Play();
	}

	public void PlayButtonTick () {

		soundButtonTick.volume = manager.soundVolume;
		soundButtonTick.Play();
	}

}