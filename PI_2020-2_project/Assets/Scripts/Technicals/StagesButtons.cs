﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class StagesButtons : MonoBehaviour {

	#region Stage completion variables

	[Header ("Completion")]

	[Tooltip ("The number of this stage")]
	public int stageNumber;
	
	[HideInInspector]
	// How many completed stars the player have
	public int starsCompleted = 0;

	#endregion

	#region Stage timing variables

	[Header ("Timing")]

	[Tooltip ("The time that the player must overcome")]
	public float parTime = 60.0f;

	[Tooltip ("The best time the player has achieved")]
	public float bestTime = 999.9f;

	[HideInInspector]
	// If the player has the time star
	public bool starTime = false;

	#endregion

	#region Stage scoring variables

	[Header ("Scoring")]

	[Tooltip("The score that the player must overcome")]
	public int parPoints = 10;

	[Tooltip ("The best score the player has achieved")]
	public int bestPoints = 0;

	[HideInInspector]
	// If the player has the points star
	public bool starPoints = false;

	#endregion

	#region Private variables

	private GameManager gameManager;
	
	private Transform[] stars = new Transform[3];

	#endregion

	#region Basic methods

	private void Awake () {
		
		AddEventTriggerHandle();
	}

	private void Start () {

		for (int i = 1; i <= stars.Length; i++) {

			stars[i-1] = transform.GetChild(i);
		}

		gameManager = GameManager.Instance;

		if (gameManager.currentLevel == stageNumber) {

			if (gameManager.currentLevelTime < bestTime) {

				bestTime = gameManager.currentLevelTime;
			}

			if (gameManager.currentLevelPoints > bestPoints) {

				bestPoints = gameManager.currentLevelPoints;
			}
		}

		if (gameManager.currentLevelProgress >= stageNumber) {

			bestPoints = PlayerPrefs.GetInt(stageNumber + "Points", 0);
			bestTime = PlayerPrefs.GetFloat(stageNumber + "Time", 999.9f);

			GetComponent<Button>().interactable = true;

			if (gameManager.currentLevelProgress > stageNumber) {
				starsCompleted = 1;
				
				if (bestTime <= parTime) {

					starsCompleted++;
				}

				if (bestPoints >= parPoints) {

					starsCompleted++;
				}
			}
		}

		for (int i = 1; i <= starsCompleted; i++) {

			stars[i-1].gameObject.SetActive(true);
		}

	}

	#endregion

	#region Events handle methods

	private void AddEventTriggerHandle () {
		
		EventTrigger.Entry entryClick = new EventTrigger.Entry();

		entryClick.eventID = EventTriggerType.PointerClick;
		entryClick.callback.AddListener((dataClick) => { OnPointerClickDelegate((PointerEventData) dataClick, gameObject); });

		GetComponent<EventTrigger>().triggers.Add(entryClick);
	}

	private void OnPointerClickDelegate (PointerEventData eventData, GameObject obj) {

		gameManager.onStage = true;
		gameManager.currentLevelTime = 0.0f;
		gameManager.currentLevelPoints = 0;
		gameManager.currentLevelBestTime = bestTime;
		gameManager.currentLevelBestPoints = bestPoints;
		gameManager.currentLevel = stageNumber;
		gameManager.currentLevelParTime = parTime;
		gameManager.currentLevelParPoints = parPoints;
	}

	#endregion
}