﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneControl : MonoBehaviour {

	public CheckpointControl startCheckpoint;

	public GameObject pauseScreen;

	private bool isPaused = false;

	#region basic methods

	private void Awake () {

		LockCursor();

		Time.timeScale = 1;

		isPaused = false;
	}

	private void Start () {

		GameManager.Instance.actualCheckpoint = startCheckpoint;
	}

	private void Update () {

		CheckCursorLock();

		CheckPause();
	}

	#endregion

	#region cursor management

	private void CheckCursorLock () {

		if (Input.GetKeyDown(KeyCode.Tab)) {

			if (Cursor.lockState == CursorLockMode.Locked) { // destrava mouse

				UnlockCursor();
			} else if (Cursor.lockState == CursorLockMode.None) { // trava mouse

				LockCursor();
			}
		}
	}

	private void LockCursor () {

		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}

	private void UnlockCursor () {

		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
	}

	#endregion

	#region pause management

	private void CheckPause () {

		if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P)) {

			TogglePause();
		}
	}

	public void TogglePause () {

		isPaused = !isPaused;

		if (isPaused == false) {

			Time.timeScale = 1;
			pauseScreen.SetActive(false);
			LockCursor();
		} else {

			Time.timeScale = 0;
			pauseScreen.SetActive(true);
			UnlockCursor();
		}
	}

	#endregion
}