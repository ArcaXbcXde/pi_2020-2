﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PlayerBallControl : MonoBehaviour {

	#region heirloom from car movement

	//#region public variables

	//[Header("Movement")]

	//[Tooltip("Strength in which the player moves the ball forward, more strength imply in more speed")]
	//[Range(1.0f, 100.0f)]
	//public float forwardMoveStr = 5.0f;

	//[Tooltip("Strength in which the player moves with the ball backwards, more strength imply in more speed")]
	//[Range(1.0f, 100.0f)]
	//public float backwardMoveStr = 2.5f;

	//[Tooltip("Rotation speed in degrees")]
	//[Range(1.0f, 1080.0f)]
	//public float rotateSpeed = 360.0f;

	//[Tooltip("The hamster to act as a fixed reference for the ground collider")]
	//public CheckGround hamsterCollider;


	//[Header("Jump")]

	//[Tooltip("If the player can jump or not")]
	//public bool jumpable = true;

	//[Tooltip("Key used for jumping")]
	//public KeyCode jumpKey = KeyCode.Space;

	//// The starting set jump key of the application
	//[HideInInspector]
	//public KeyCode defaultJumpKey;

	//[Tooltip("Jump strength")]
	//[Range(1.0f, 1000.0f)]
	//public float jumpStr = 100.0f;

	//[Tooltip("The velocity which the player falls (may affect world)")]
	//public Vector3 playerGravity = new Vector3(0.0f, 15.0f, 0.0f);


	//[Header("Dash")]

	//[Tooltip("If the player can dash with the ball")]
	//public bool dashable = true;

	//[Tooltip("Key used for dashing")]
	//public KeyCode dashKey = KeyCode.LeftShift;

	//// The starting set dash key of the application
	//[HideInInspector]
	//public KeyCode defaultDashKey;

	//[Tooltip("Time in seconds to recharge the dash")]
	//[Range(0.01f, 20.0f)]
	//public float timeToDash = 2.0f;

	//[Tooltip("Dash strength")]
	//[Range(100.0f, 10000.0f)]
	//public float dashStr = 1000.0f;


	//[Header("Drift")]

	//[Tooltip("If the player can press a key to drift")]
	//public bool driftable = true;

	//[Tooltip("Key used for drifting")]
	//public KeyCode driftKey = KeyCode.LeftControl;

	//// The starting set drift key of the application
	//[HideInInspector]
	//public KeyCode defaultDriftKey;

	//[Tooltip("The minimum speed to start a drift")]
	//[Range(0.0f, 100.0f)]
	//public float minimumSpd = 1.0f;

	//[Tooltip("percentage of player speed maintained during the drift")]
	//[Range(0.0f, 0.99f)]
	//public float driftSpeedMaintainPercent = 0.80f;

	//[Tooltip("The base rotation speed during drift")]
	//[Range(0.0f, 1080.0f)]
	//public float driftRotationSpeed = 30.0f;

	//[Tooltip("The influence the player have over the drift rotation speed (preferably always lower than 'driftRotationSpeed')")]
	//[Range(0.0f, 1080.0f)]
	//public float driftRotationInfluence = 15.0f;

	//[Tooltip("The escape force during drift (force applied at the side of the player to maintain the drift sideways)")]
	//[Range(0.0f, 100.0f)]
	//public float escapeStr = 3.0f;

	//// A flag to mark if the player is drifting in each frame
	//[HideInInspector]
	//public bool drifting = false;

	//#endregion

	//#region private variables

	//// made to easily get if the player is on ground
	//private bool onGround;

	//// buffer on Update to use to jump on FixedUpdate
	//private bool jumpBuffer = false;

	//// buffer on Update to use to dash on FixedUpdate
	//private bool dashBuffer = false;

	//// Basic unity vertical axis control variable, easily used for animations and shortening calls
	////[SerializeField]
	//private float movX;

	//// Basic unity horizontal axis control variable, easily used for animations and shortening calls
	////[SerializeField]
	//private float movZ;

	//// For strength buff/debuff changes and be able to go back to the original move strength
	//private float originalMoveStr;

	//// Used in calculations for how much the player should move forward/backwards this frame
	//private float inputStr;

	//// Used in calculations for how much the player should rotate this frame
	//private float inputTurn;

	//// Time left to be possible to dash again
	//private float cdDash;

	//// Initial speed of the drift, reduced by the "driftSpeedMaintainPercent"
	////[SerializeField]
	//private float driftStartSpeed;

	//// Define if the ball is drifting to its left or its right (so i don't need to declare a new enum)
	////[SerializeField]
	//private TextAlignment driftDirection;

	//// Strength to be applied in the player's rigidbody
	//private Vector3 moveStrengthApplied;

	//// Escape strength after calculations ready to be applied on drift
	//private Vector3 driftStr;

	//// To check what is the ground layer for the raycast
	////private LayerMask whatIsGround;

	//// Amount turned in an update
	//private Quaternion turningAmount;

	//private GameManager manager;

	//// To ease checks for camera rotation
	//private Transform camTransform;

	//// A fixed reference for the player since its not child of its holder during play
	//private Transform playerItself;

	//// The player's rigidbody
	//private Rigidbody rigid;

	//#endregion

	//#region basic methods

	//private void Awake () {

	//	defaultJumpKey = jumpKey;
	//	defaultDashKey = dashKey;
	//	defaultDriftKey = driftKey;

	//	Physics.gravity = -playerGravity;

	//	//whatIsGround = ~LayerMask.NameToLayer("Ground");

	//	camTransform = Camera.main.transform;

	//	originalMoveStr = forwardMoveStr;

	//	playerItself = transform.GetChild(0);
	//	playerItself.GetComponent<Player>().myHolder = transform;

	//	rigid = playerItself.GetComponent<Rigidbody>();
	//	rigid.transform.parent = null;

	//}

	//private void Start () {

	//	manager = GameManager.Instance;

	//	if (manager.jumpKey != KeyCode.None) {

	//		jumpKey = manager.jumpKey;
	//	}

	//	if (manager.dashKey != KeyCode.None) {

	//		dashKey = manager.dashKey;
	//	}

	//	if (manager.driftKey != KeyCode.None) {

	//		driftKey = manager.driftKey;
	//	}
	//}

	//private void Update () {

	//	if (onGround == true) {

	//		CheckInputs();
	//	} else {

	//		NoInputs();
	//	}

	//	if (turningAmount != new Quaternion(0, 0, 0, 0)) {

	//		DoRotation();
	//	}

	//	// can be substituted for a coroutine that can be called every "timeToDash" seconds
	//	DoCountDowns();
	//}

	//// Mainly to control player actions
	//private void FixedUpdate () {

	//	CheckGround();

	//	// ball movement inputs
	//	if (onGround == true) {

	//		CheckWalk();

	//		CheckJump();

	//		CheckDash();
	//	}

	//	if (moveStrengthApplied != new Vector3(0, 0, 0)) {

	//		DoMovement();
	//	}
	//}

	//private void LateUpdate () {

	//	transform.position = rigid.transform.position;
	//}

	//#endregion

	//#region check methods

	///* Method that checks inputs, if the player is drifting
	// * and the strength of each input to buffer jump or dash
	// * to use in the next fixed update and apply movement accordingly
	// */
	//private void CheckInputs () {

	//	// Check axis inputs
	//	movX = Input.GetAxis("Horizontal");
	//	movZ = Input.GetAxis("Vertical");

	//	CheckDrift();

	//	// Still very optimizable
	//	if (drifting == false) {

	//		// Move front/back
	//		if (movZ > 0) {

	//			inputStr = movZ * forwardMoveStr;
	//		} else if (movZ < 0) {

	//			inputStr = movZ * backwardMoveStr;
	//		}

	//		// Move left/right calculating the amount of input over the horizontal axis, the base rotation speed and delta time
	//		inputTurn = movX * rotateSpeed * Time.deltaTime;

	//		// Calculate rotation using "inputTurn"
	//		turningAmount = Quaternion.Euler(transform.rotation.eulerAngles + (transform.up * inputTurn));

	//		// Control jump
	//		if ((jumpable == true) && (Input.GetKey(jumpKey) == true)) {

	//			jumpBuffer = true;
	//		}

	//		// Control dash
	//		if ((dashable == true) && (Input.GetKeyDown(dashKey)) && (cdDash <= 0)) {

	//			dashBuffer = true;
	//		}
	//	} else if (drifting == true) {

	//		// Constant forward speed during drift, but not so fast
	//		inputStr = driftStartSpeed * driftSpeedMaintainPercent;

	//		if (driftDirection == TextAlignment.Left) {

	//			inputTurn = (-driftRotationSpeed + (movX * driftRotationInfluence)) * Time.deltaTime;
	//			driftStr = (transform.right * (escapeStr));
	//		} else if (driftDirection == TextAlignment.Right) {

	//			inputTurn = (driftRotationSpeed + (movX * driftRotationInfluence)) * Time.deltaTime;
	//			driftStr = (transform.right * (-escapeStr));
	//		}
	//		// Do rotation using "inputTurn" calculation (probably shouldn't be here)
	//		turningAmount = Quaternion.Euler(transform.rotation.eulerAngles + (transform.up * inputTurn));

	//	}
	//}

	///* Check if the player can drift, if its drifting
	// * and to which side he is drifting
	// * Act as a complementary method for the "CkeckInputs()" method
	// */
	//private void CheckDrift () {

	//	if (driftable == true) {
	//		if ((Input.GetKey(driftKey)) && (rigid.velocity.magnitude > minimumSpd)) {
	//			if (drifting == false && Mathf.Abs(movZ) > 0.5f) {
	//				if (movX > 0.3f) {

	//					driftDirection = TextAlignment.Right;
	//					driftStartSpeed = rigid.velocity.magnitude;
	//					drifting = true;
	//				} else if (movX < -0.3f) {

	//					driftDirection = TextAlignment.Left;
	//					driftStartSpeed = rigid.velocity.magnitude;
	//					drifting = true;
	//				} else {
	//					driftDirection = TextAlignment.Center;
	//				}
	//			}
	//		} else {

	//			drifting = false;
	//		}
	//	}
	//}

	///* Simple method to say if the player is on ground
	// * by checking if the hamster trigger is touching anything tagged with the ground tag
	// * and change the player value
	// */
	//private void CheckGround () {

	//	onGround = hamsterCollider.IsOnGround();
	//}

	///* Simple method to check if the player wants to walk
	// * and set the "moveStrengthApplied" vector z direction with the "inputStr" strength value
	// */
	//private void CheckWalk () {

	//	if ((Mathf.Abs(inputStr) > 0) || driftStr.magnitude != 0) {

	//		moveStrengthApplied = (transform.forward * inputStr) + driftStr;
	//	}
	//}

	///* Simpe method to check if jump is buffered
	// * then add "jumpStr" strength value on the "moveStrengthApplied" vector on the y direction
	// * then unbuffer the jump
	// */
	//private void CheckJump () {

	//	if (jumpBuffer == true) {

	//		moveStrengthApplied += transform.up * jumpStr;
	//		jumpBuffer = false;
	//	}
	//}

	///* Simple method to check if dash is buffered
	// * and add at the "moveStrengthApplied" vector z direction the "dashStr" strength value,
	// * puts the dash "cdDash" cd in "timeToDash" seconds cd
	// * then unbuffer the dash
	// */
	//private void CheckDash () {

	//	if (dashBuffer == true) {

	//		moveStrengthApplied += transform.forward * dashStr;
	//		cdDash = timeToDash;
	//		dashBuffer = false;
	//	}
	//}

	//#endregion

	//#region do methods

	//// Simple method called when the action keys are updated
	//public void UpdateKeys () {

	//	jumpKey = manager.jumpKey;
	//	dashKey = manager.dashKey;
	//	driftKey = manager.driftKey;
	//}

	//// If there is no inputs at the moment resets all movement-related variables
	//private void NoInputs () {

	//	movX = 0;

	//	movZ = 0;

	//	inputStr = 0;

	//	drifting = false;
	//	jumpBuffer = false;
	//	dashBuffer = false;
	//}

	//// Simple method to apply "moveStrengthApplied" strength at the player, then zeroes it
	//private void DoMovement () {

	//	rigid.AddForce(moveStrengthApplied);
	//	moveStrengthApplied = new Vector3(0.0f, 0.0f, 0.0f);
	//	driftStr = new Vector3(0.0f, 0.0f, 0.0f);
	//}

	//// Simple method to do rotation using the "turningAmount" calculation
	//private void DoRotation () {

	//	transform.rotation = turningAmount;
	//}

	//private void DoCountDowns () {

	//	if (cdDash > 0) {

	//		cdDash -= Time.deltaTime;
	//	}
	//}

	//#endregion

	//#region utility methods

	//// Used to change the player move strength on the run
	//public void SetMoveStr (float newStr, float changeTime) {
	//	forwardMoveStr = newStr;
	//	Invoke("ReturnMoveStr", changeTime);
	//}

	//// Created to avoid the use of corroutines
	//private void ReturnMoveStr () {
	//	forwardMoveStr = originalMoveStr;
	//}

	//private Vector3 GetCameraForward () {

	//	Vector3 camForward = camTransform.forward;
	//	camForward.y = 0;
	//	camForward.Normalize();

	//	return camForward;
	//}

	//private Vector3 GetCameraRight () {

	//	Vector3 camRight = camTransform.right;
	//	camRight.y = 0;
	//	camRight.Normalize();

	//	return camRight;
	//}

	//#endregion

	#endregion

	#region public variables

	public CinemachineFreeLook cinemachineCam;

	[Header("Movement")]

	[Tooltip("Strength in which the player moves the ball forward, more strength imply in more speed")]
	[Range(1.0f, 100.0f)]
	public float forwardMoveStr = 5.0f;

	[Tooltip("Strength in which the player moves with the ball backwards, more strength imply in more speed")]
	[Range(1.0f, 100.0f)]
	public float backwardMoveStr = 2.5f;

	[Tooltip ("Strength in which the player moves the ball sideways, more strength imply in more speed")]
	[Range (0.0f, 100.0f)]
	public float sidewaysMoveStr = 3.0f;

	[Tooltip("Rotation speed in degrees")]
	[Range(1.0f, 1080.0f)]
	public float rotateSpeed = 360.0f;


	[Header("Jump")]

	[Tooltip("If the player can jump or not")]
	public bool jumpable = true;

	[Tooltip("Key used for jumping")]
	public KeyCode jumpKey = KeyCode.Space;

	// The starting set jump key of the application
	[HideInInspector]
	public KeyCode defaultJumpKey;

	[Tooltip("Jump strength")]
	[Range(1.0f, 1000.0f)]
	public float jumpStr = 100.0f;

	[Tooltip("The velocity which the player falls (may affect world)")]
	public Vector3 playerGravity = new Vector3(0.0f, 15.0f, 0.0f);


	[Header("Dash")]

	[Tooltip("If the player can dash with the ball")]
	public bool dashable = true;

	[Tooltip("Key used for dashing")]
	public KeyCode dashKey = KeyCode.LeftShift;

	// The starting set dash key of the application
	[HideInInspector]
	public KeyCode defaultDashKey;

	[Tooltip("Time in seconds to recharge the dash")]
	[Range(0.01f, 20.0f)]
	public float timeToDash = 2.0f;

	[Tooltip("Dash strength")]
	[Range(100.0f, 10000.0f)]
	public float dashStr = 1000.0f;


	[Header ("Sounds")]

	[Tooltip ("Jumping sound")]
	public AudioSource soundJump;

	#endregion

	#region private variables
	
	// made to easily get if the player is on ground
	private bool onGround;

	// buffer on Update to use to jump on FixedUpdate
	private bool jumpBuffer = false;

	// buffer on Update to use to dash on FixedUpdate
	private bool dashBuffer = false;

	// Basic unity vertical axis control variable, easily used for animations and shortening calls
	private float movX;

	// Basic unity horizontal axis control variable, easily used for animations and shortening calls
	private float movZ;

	// Basic unity mouse axis control variable, easily used for animations and shortening calls
	private float mouseX;

	// For strength buff/debuff changes and be able to go back to the original move strength
	private float originalMoveStr;

	// Used in calculations for how much the player should move forward/backwards in this frame
	private float inputStrZ;

	// Used in calculations for how much the player should move sideways in this frame
	private float inputStrX;

	// Used in calculations for how much the player should rotate this frame
	private float inputTurn;

	// Time left to be possible to dash again
	private float cdDash;
	
	// Strength to be applied in the player's rigidbody
	private Vector3 moveStrengthApplied;
	
	private GameManager manager;
	
	// A fixed reference for the player (the ball)
	private Transform playerItself;

	// A fixed reference for the hamster
	private CheckGround hamsterItself;

	// The player's rigidbody
	private Rigidbody rigid;

	#endregion

	#region basic methods

	private void Awake () {

		defaultJumpKey = jumpKey;
		defaultDashKey = dashKey;

		Physics.gravity = -playerGravity;
		
		originalMoveStr = forwardMoveStr;

		playerItself = transform.GetChild(0);
		playerItself.GetComponent<Player>().myHolder = transform;

		hamsterItself = transform.GetChild(1).GetComponent<CheckGround>();

		rigid = playerItself.GetComponent<Rigidbody>();
		rigid.transform.parent = null;
	}

	private void Start () {
		
		manager = GameManager.Instance;

		if (manager.jumpKey != KeyCode.None) {

			jumpKey = manager.jumpKey;
		}

		if (manager.dashKey != KeyCode.None) {

			dashKey = manager.dashKey;
		}
	}

	private void Update () {

		// Movement Control
		if (onGround == true) {

			CheckInputs();
		} else {

			NoInputs();
		}

		// Turning control
		CheckTurning();

		if (inputTurn != 0) {

			DoRotation();
		}

		// can be substituted for a coroutine that can act every "timeToDash" seconds
		DoCountDowns();
	}

	// Mainly to control player actions
	private void FixedUpdate () {

		CheckGround();

		// ball movement inputs
		if (onGround == true) {

			CheckWalk();

			CheckJump();

			CheckDash();
		}

		if (moveStrengthApplied != Vector3.zero) {

			DoMovement();
		}
	}

	private void LateUpdate () {

		DoAnimation();
		
		transform.position = rigid.transform.position;
	}

	#endregion

	#region check methods

	private void CheckInputs () {

		//Input de movimento da bola
		movX = Input.GetAxis("Horizontal");
		movZ = Input.GetAxis("Vertical");

		hamsterItself.GetComponent<Animator>().SetFloat("MovSpd", movZ);

		// Move front/back
		if (movZ > 0) {

			inputStrZ = movZ * forwardMoveStr;
		} else if (movZ < 0) {

			inputStrZ = movZ * backwardMoveStr;
		}

		inputStrX = movX * sidewaysMoveStr;
		
		// Control jump
		if ((jumpable == true) && (Input.GetKey(jumpKey) == true)) {

			jumpBuffer = true;
		}

		// Control dash
		if ((dashable == true) && (Input.GetKeyDown(dashKey)) && (cdDash <= 0)) {

			dashBuffer = true;
		}

	}

	private void CheckTurning () {

		mouseX = Input.GetAxis("Mouse X");

		inputTurn = mouseX * rotateSpeed * Time.deltaTime * manager.cameraSensitivity;

		if (manager.invertXAxis == true) {

			inputTurn *= -1;
		}
	}

	private void CheckWalk () {

		if ((Mathf.Abs(inputStrZ) > 0) || (Mathf.Abs(inputStrX) > 0)) {

			moveStrengthApplied = (transform.forward * inputStrZ) + (transform.right * inputStrX);
		}
	}

	private void CheckJump () {

		if (jumpBuffer == true) {

			moveStrengthApplied += transform.up * jumpStr;
			jumpBuffer = false;

			soundJump.volume = manager.soundVolume;
			soundJump.Play();
		}
	}

	private void CheckDash () {

		if (dashBuffer == true) {

			moveStrengthApplied += transform.forward * dashStr;
			cdDash = timeToDash;
			dashBuffer = false;
		}
	}

	/* Simple method to say if the player is on ground
	 * by checking if the hamster trigger is touching anything tagged with the ground tag
	 * and change the player value
	 */
	private void CheckGround () {

		onGround = hamsterItself.IsOnGround();
	}

	#endregion

	#region do methods

	// Simple method called when the action keys are updated
	public void UpdateKeys () {

		jumpKey = manager.jumpKey;
		dashKey = manager.dashKey;
	}

	// If there is no inputs at the moment resets all movement-related variables
	private void NoInputs () {

		movX = 0;

		movZ = 0;

		inputStrZ = 0;
		
		jumpBuffer = false;
		dashBuffer = false;
	}

	// Simple method to apply "moveStrengthApplied" strength at the player, then zeroes it
	private void DoMovement () {

		rigid.AddForce(moveStrengthApplied);
		moveStrengthApplied = Vector3.zero;
	}

	// !! --> Need to add an Lerp to smoothen rotation, at the moment it can be too jittery <---------------------------------- !!
	// Simple method to rotate the hamster using the "turningAmount" calculation
	private void DoRotation () {
		
		transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles + (transform.up * inputTurn));

		cinemachineCam.m_XAxis.Value = transform.rotation.eulerAngles.y;
	}

	private void DoAnimation () {
		
		Debug.Log ("movZ: " + movZ);
	}

	private void DoCountDowns () {

		if (cdDash > 0) {

			cdDash -= Time.deltaTime;
		}
	}

	#endregion

	#region utility methods

	// Used to change the player move strength on the run
	public void SetMoveStr (float newStr, float changeTime) {

		forwardMoveStr = newStr;
		Invoke("ReturnMoveStr", changeTime);
	}

	// Created to avoid the use of corroutines
	private void ReturnMoveStr () {

		forwardMoveStr = originalMoveStr;
	}

	#endregion
}