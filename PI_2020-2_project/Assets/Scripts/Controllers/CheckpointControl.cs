﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointControl : MonoBehaviour {

	public bool toggled;

	private GameManager dump;

	private Animator anima;

	private void Awake () {
		
		anima = GetComponentInChildren<Animator>();
	}

	private void Start () {
		
		dump = GameManager.Instance;
	}

	public void SetCheckpoint () {

		// desativa o checkpoint passado
		dump.actualCheckpoint.toggled = false;
		dump.actualCheckpoint.anima.SetBool ("Toggled", false);

		// diz que agora esse é o checkpoint
		dump.actualCheckpoint = gameObject.GetComponent<CheckpointControl>();

		// ativa esse checkpoint
		toggled = true;
		anima.SetBool("Toggled", true);

		//efeitos de checkpoint alcançado aqui
	}
	
	private void OnTriggerEnter (Collider col) {
		
		if (col.tag == "Player" && toggled == false){

			SetCheckpoint();
		}
	}
}