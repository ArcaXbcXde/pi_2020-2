﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPosition : MonoBehaviour {

	private Vector3 startingPos;

	private Rigidbody rigid;

	private void Awake () {

		rigid = GetComponent<Rigidbody>();
		startingPos = transform.position;
	}

	private void Update () {
		// If fall out of ground goes back to spawn position
		if (transform.position.y < -2) {
			ResetPos();
		}
	}

	public void ResetPos () {

		rigid.velocity = Vector3.zero;
		rigid.angularVelocity = Vector3.zero;
		transform.position = startingPos;
	}
}