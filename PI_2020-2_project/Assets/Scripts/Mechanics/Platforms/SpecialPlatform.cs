﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SpecialPlatform : MonoBehaviour {

	[Tooltip("The strength of the push")]
	[Range(10.0f, 10000.0f)]
	public float strength = 1000.0f;

	protected Transform forceTowardsHere;

	protected virtual void Awake () {

		forceTowardsHere = transform.GetChild(0);
	}

	protected virtual void OnTriggerEnter (Collider col) {
		if (col.tag == "Player") {

			// Delete with PlayerCarControl
			if (col.GetComponent<Player>().myHolder.GetComponent<PlayerCarControl>() != null) {

				col.GetComponent<Player>().myHolder.GetComponent<PlayerCarControl>().drifting = false;
			}
			
			PlatformEffect(col);
		}
	}

	protected abstract void PlatformEffect (Collider col);
}