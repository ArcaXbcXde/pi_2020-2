﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoppingPlatform : SpecialPlatform {

	protected override void PlatformEffect (Collider col) {

			col.GetComponent<Rigidbody>().velocity = Vector3.zero;
			col.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
	}
}