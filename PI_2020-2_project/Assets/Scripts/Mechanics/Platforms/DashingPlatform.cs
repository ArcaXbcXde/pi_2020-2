﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashingPlatform : SpecialPlatform {

	protected override void PlatformEffect (Collider col) {

			col.GetComponent<Rigidbody>().AddForce ((forceTowardsHere.position - transform.position).normalized * strength);
	}
}