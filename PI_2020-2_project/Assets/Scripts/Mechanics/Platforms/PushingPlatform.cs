﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushingPlatform : SpecialPlatform {
	
	private void OnTriggerStay (Collider col) {
		if (col.tag == "Player") {

			PlatformEffect(col);
		}
	}

	protected override void PlatformEffect (Collider col) {
		
		col.GetComponent<Rigidbody>().AddForce((forceTowardsHere.position - transform.position).normalized * strength);
	}
}
