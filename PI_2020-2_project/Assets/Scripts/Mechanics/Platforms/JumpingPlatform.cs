﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpingPlatform : SpecialPlatform {
	
	public AudioSource soundSpring;

	protected override void PlatformEffect (Collider col) {

		col.GetComponent<Rigidbody>().AddForce(Vector3.up * (strength - col.GetComponent<Rigidbody>().velocity.y));

		soundSpring.volume = GameManager.Instance.soundVolume;
		soundSpring.Play();
	}
}