﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour {

	public enum PlatformType{
		TowardsAngle,
		Rotating,
		Disappearing,
		Spinning,
	}

	public float timeToMove;

	public PlatformType platformType = PlatformType.TowardsAngle;

	public Vector3 movingSpeed;

	private Vector3 startPosition;
	private Quaternion startRotation;

	private Rigidbody rigid;


	private void Awake () {

		startPosition = transform.position;
		startRotation = transform.rotation;

		rigid = GetComponent<Rigidbody>(); 
	}

	private void FixedUpdate () {
		
		if (platformType == PlatformType.TowardsAngle) {


		} else if (platformType == PlatformType.Rotating) {


		} else if (platformType == PlatformType.Disappearing) {


		} else if (platformType == PlatformType.Spinning) {


		}
	}
}