﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : MonoBehaviour {

	[SerializeField]
	protected float rotateSpeed = 1;

	protected Player playerComponent;


	protected virtual void LateUpdate () {

		transform.RotateAround(transform.position, new Vector3 (0, 1, 0), rotateSpeed);      //gira rotateSpeed° no eixo Y a cada frame
	}

	protected virtual void OnTriggerEnter (Collider col) {

		//Ao ser coletado, adiciona 1 à pontuação e é destruído
		if (col.tag == "Player") {

			if (playerComponent == null) {

				playerComponent = col.GetComponent<Player>();
			}

			playerComponent.AddPoint();
			
			SoundManager.Instance.soundSeed.Play();

			Destroy(gameObject);
		}
	}
}