﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingDoor : Mechanicals {
	
	[Tooltip ("Speed in degrees/s which it rotates")]
	[Range(0.1f, 720.0f)]
	public float rotationSpeed = 5.0f;

	[Tooltip ("How much it is supposed to rotate (in Z axis), to change spin direction use a negative number")]
	public float moveRotation;

	// The starting rotation of this object
	private float initialRotation;

	private void Awake () {

		initialRotation = transform.rotation.z;
	}

	// Starts rotating this object using the set "moveRotation" rotation
	protected override void OnActivate () {

		StartCoroutine(CorroutineRotateObject (true, initialRotation + moveRotation));
	}

	// Starts rotating this object to the starting rotation
	protected override void OnDeactivate () {

		StartCoroutine(CorroutineRotateObject(false, initialRotation));
	}

	// Corroutine to start rotating the object to the set "rotateToPosition" rotation
	private IEnumerator CorroutineRotateObject (bool actualActivation, float rotateToPosition) {

		Vector3 rotationDirection;

		// Makes the wall spin clockwise or counter-clockwise depending of the angle to be added being positive or negative
		if (rotateToPosition > initialRotation) {
			
			rotationDirection = Vector3.forward * rotationSpeed;

		} else {

			rotationDirection = Vector3.back * rotationSpeed;
		}

		// eulerAngles goes from 0 to 360, there is no negative rotation so the door will spin forever trying to reach a negative angle... this solves the issue
		if (rotateToPosition < 0.0f) {

				rotateToPosition *= -1.0f;
		}

		while ((Mathf.Abs(transform.rotation.eulerAngles.z - rotateToPosition) > rotationSpeed * Time.deltaTime) && (isActivated == actualActivation)) {
			transform.Rotate(rotationDirection * Time.deltaTime);

			yield return new WaitForSeconds(Time.deltaTime);
		}

		transform.Rotate (Vector3.back * (transform.rotation.eulerAngles.z - rotateToPosition));
	}
}