﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : Mechanicals {
	
	public float shootStr;

	private Rigidbody objectToShoot;

	private Transform reference;

	private void Awake () {

		reference = transform.GetChild(0);
	}

	protected override void OnActivate () {
		Invoke ("Shoot", timeUntilActivate);
	}

	protected override void OnDeactivate () {}

	private void Shoot () {

		if (objectToShoot != null) {

			objectToShoot.AddForce((reference.position - transform.position).normalized * shootStr);
			objectToShoot = null;
		} else {
			Debug.Log ("No object ready to shoot");
		}
	}

	private void OnTriggerEnter (Collider col) {
		
		if (((col.tag == "Player") || (col.tag == "Prop")) && objectToShoot == null) {

			objectToShoot = col.GetComponent<Rigidbody>();
		}
	}

	private void OnTriggerExit (Collider col) {

		if (col.GetComponent<Rigidbody>() == objectToShoot) {
			objectToShoot = null;
		}
	}
}
