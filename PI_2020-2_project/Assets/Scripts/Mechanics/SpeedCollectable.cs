﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedCollectable : Collectable {

	private float originalStrBall;
	private float originalStrCar;

	[Range (5.0f, 100.0f)]
	public float strToAdd = 10.0f;

	[Range (1.0f, 120.0f)]
	public float buffTime = 5.0f;

	[Range  (1.0f, 120.0f)]
	public float respawnTime = 10.0f;
	
	public PlayerBallControl playerControlComponentBall;
	
	private MeshRenderer meshRenderer;
	private Collider thisCollider;

	private void Awake () {

		meshRenderer = GetComponent<MeshRenderer>();
		thisCollider = GetComponent<Collider>();
	}

	private void Start () {

		if (playerControlComponentBall.enabled == true) {

			originalStrCar = playerControlComponentBall.forwardMoveStr;
		}
	}

	protected override void OnTriggerEnter (Collider col) {

		//Ao ser coletado, aumenta a força de movimento do jogador em "strToAdd" e some, retorna após "respawnTime" tempo
		if (col.tag == "Player") {
			
			if (playerControlComponentBall.enabled == true) {

				playerControlComponentBall.SetMoveStr(originalStrCar + strToAdd, buffTime);
			}

			DeactivateCollectable();
			Invoke ("ReactivateCollectable", respawnTime);
			//Destroy(gameObject);
		}
	}

	private void DeactivateCollectable () {

		meshRenderer.enabled = false;
		thisCollider.enabled = false;
	}

	private void ReactivateCollectable () {

		meshRenderer.enabled = true;
		thisCollider.enabled = true;
	}
}